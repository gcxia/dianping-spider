# -*- coding: utf-8 -*-
import json

import re
from lxml import html
from scrapy import FormRequest
from scrapy.spiders import Spider
from urllib import unquote


class DianpingSpider(Spider):
    name = 'DianpingSpider'
    custom_settings = {
        "DEFAULT_REQUEST_HEADERS": {
            'accept': 'application/json, text/javascript, */*; q=0.01',
            'accept-encoding': 'gzip, deflate',
            'accept-language': 'zh-CN,zh;q=0.8,en;q=0.6,zh-TW;q=0.4',
            'Content - Type': 'application / x - www - form - urlencoded;charset = UTF - 8',
            'Proxy - Connection': 'keep - alive',
            'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
            'x-requested-with': 'XMLHttpRequest',
        }}

    def start_requests(self):
        vaild_url = 'https://www.dianping.com/search/category/1/10'
        vaild_requests = []

        vaild_request = FormRequest(vaild_url, method="GET", callback=self.parse_detail)
        vaild_requests.append(vaild_request)
        return vaild_requests

    def parse_detail(self, response):
        html_content = html.fromstring(response.body)
        li_list = html_content.xpath('//div[@id="shop-all-list"]/ul[1]/li')
        vaild_requests = []
        for item in li_list:
            item_href = item.xpath('./div[@class="txt"]/div[@class="tit"]/a[1]//@href')
            vaild_request = FormRequest('https://www.dianping.com' + item_href[0], method="GET",
                                        callback=self.parse_position)
            vaild_requests.append(vaild_request)

        return vaild_requests

    def parse_position(self, response):
        html_content = response.body
        __regex = re.compile('window\.\s*shop_config\s*=\s*\{.*?shopGlat\s*:\s*"(.*?)".*?shopGlng\s*:\s*"(.*?)".*?\}',
                             re.S)
        position = re.findall(__regex, html_content)
        print position
